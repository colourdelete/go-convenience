package pkg

import "reflect"

type IPromise interface {
	ReturnType() reflect.Type
	Await() reflect.Value
}

type Promise struct {
	f func(chan reflect.Value) reflect.Value
	t reflect.Type
	c chan reflect.Value
	r *reflect.Value
}

func NewPromise(f func(chan reflect.Value) reflect.Value, t reflect.Type) IPromise {
	c := make(chan reflect.Value)
	r := &reflect.Value{}
	go f(c)
	go func() {
		rC := <- c // resultCache
		r = &rC
	}()
	return &Promise{
		f: f,
		t: t,
		c: c,
		r: r,
	}
}

func (p *Promise) ReturnType() reflect.Type {
	return p.t
}

func (p *Promise) Done() bool {
	return p.r != nil
}

func (p *Promise) Await() reflect.Value {
	for p.r == nil {}
	return reflect.ValueOf(p.c)
}
